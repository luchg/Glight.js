﻿#轻量级JS模块化加载管理
Glight.js是一款遵循CMD规范的轻量级js模块化加载款里框架，你可以很轻松的将NodeJS、RequireJS、SeaJS的模块转移过来使用，有些甚至直接兼容！

jQuery的本遵循AMD的规范，但它兼容NodeJS，因此，我们只需这么写即可轻松让jQuery成为Glight.js的模块：
```
define(function(require, exports, module){
	// jQuery源代码
});
```
有些模块框架(如artTemplate模板引擎)，则无需任何修改直接兼容！

##Api
<https://git.oschina.net/luchg/Glight.js/wikis/api>

##License
1.1.2版以后，Glight.js遵循[MIT](http://opensource.org/licenses/MIT)协议发布


















***
##更新日志
###1.1.2
`2015-01-27`

* 本次版本以更宽松的MIT协议发布
* 更换Glight.js路径获取方式，增加支持使用id="glight-js-node"以提高Glight.js路径获取的准确性，不写id也是支持的，但是不支持没有id的情况下使用异步加载Glight.js
* 修复Glight.js不在head标签时在IE下路径错误的问题
* 修复IE10下路径读取错误的问题
* 修复Safari下无法正常运行的BUG
* 修复模块再依赖模块等多层次依赖时候依赖关系没加载完成模块就已完成加载的问题
* 修复debug模式在包含console的低版本ie等奇葩浏览器下出现异常的问题



###1.1.1
`2015-01-21`

* 修复编写模块时，define闭包内的this不为window的问题



###1.1.0
`2014-12-31`

* 遵循CMD规范进行大面积重构，大致修改如下：
    * 重构define方法，参数遵循CMD规范变更为(require, exports, module)，require不允许修改参数名，后两者允许修改但是不建议
	* 重构模块加载逻辑，不再进行预编译，统一在require时候才编译
	* html中不再支持define方法加载模块，应当使用use方法引入主模块
	* 重构模块加载路径逻辑，统一采用相对当前模块
	* base仅作用于use方法，同时缺省为相对GlightJS.js文件路径，也就是base缺省时use引入的模块路径使用相对路径时候是相对于GlightJS.js路径
	* 别名设置等统一进入配置方法设置 GlightJS.config，同时增加vars自定义变量设置、文件编码设置
	* 自动补加的.js后缀可通过suffix配置项自定义
	* 增加开启调试模式，开启后可以在支持console的浏览器下看到文件加载、编译情况，Chrome浏览器最佳，IE下略显畸形，调试模式下不清除js加载时在html里留下的痕迹
	* 开放缓存查看接口，本模块是为开发者设计的，作者认为将缓存理应开放给开发者们查阅以方便调试



###[1.0.4](https://git.oschina.net/luchg/Glight.js/tree/v1.0.4)
`2014-12-18`

* 修复在IE下控制台监视文件加载时候会多显示出几条请求的问题(此BUG只额外多显示，不影响正常工作)



###[1.0.3](https://git.oschina.net/luchg/Glight.js/tree/v1.0.3)
`2014-12-16`

* 修复CSS文件加载在IE7以下的一处BUG



###1.0.2
`2014-12-16`

 * 新增设置包内base路径时候支持使用 {:this:} 设置base路径为当前模块所在路径，妈妈再也不用担心我在模块内require时候遇到路径问题了，不过 base("{:this:}") 虽然是可以设置base路径为当前模块所在的路径，但是它的返回值并非是模块所在的路径，因此，如需取到当前模块的base路径，请使用 base() 方法，也就是“不传参”，注： {:this:} 方法仅对包内base有效，详细用法见api文档




###1.0.1
`2014-12-12`

* 新增GlightJS.alias模块别名设置，详情api文档



###1.0.0
`2014-11-21`

* 全新GlightJS模块化js加载管理框架诞生