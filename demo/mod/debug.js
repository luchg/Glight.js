/**
 * Glight.js在不支持console的浏览器下调试用
 */
!function(){
	if(typeof(window.console)!="object" || typeof(window.console.log)!="function"){
		var css = document.createElement('style');
			css.type = 'text/css';
			document.getElementsByTagName('head')[0].appendChild(css);
		if (css.styleSheet) {
			css.styleSheet.cssText = '#console-log{border: 1px dotted #ccc; padding: 10px; font-size: 12px; font-family: Arial; }'
									+'#console-log a{color: #888;text-decoration: none;}'
									+'#console-log a:hover{text-decoration: underline;}'
									+'#console-log p{margin: 0; }';
		} else {
			css.appendChild(document.createTextNode(str));
		}
		var div = document.createElement("div");
		div.id = "console-log";
		document.body.appendChild(div);
		console = {
			log:function(m,c){
				m = m.replace(/%c|%O/g,"").match(/^(.+?:)(.+)$/);
				div.innerHTML  +='<p>'
								+	'<span style="'+c+'">'+m[1]+'</span>'
								+	'<a target="_blank" href="'+m[2]+'">'+m[2]+'</a>'
								+'</p>'
			}
		};
	}
	GlightJS.config("debug",true);
	define(function(){
		return window.console;
	});
}();