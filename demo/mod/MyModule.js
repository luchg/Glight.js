define(function(require, exports, module){
	var mod  = module.exports = {},
		json = require("data");
	mod.hello = function(){
		alert(json.hello);
	};
	mod.getInfo = function(){
		return "This is my first module!";
	};
});