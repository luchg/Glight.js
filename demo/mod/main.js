define(function(require, exports, module){
	var m = require("MyModule");
	m.hello();
	var $ = require("jquery");		// 鉴于大家都喜欢jQuery，这里就不例外的弄个jQuery的模块，很简单的
	$("#show").html(m.getInfo());

	var T = require("Time");
	var html = $(
			 '<p>现在时间：<span class="J_gz"></span>&nbsp;&nbsp;<span class="J_date"></span>&nbsp;&nbsp;<span class="J_leap"></span></p>'
			+'<p>生肖：<span class="J_sx"></span>&nbsp;&nbsp;&nbsp;&nbsp;星座：<span class="J_xz"></span></p>'
			+'<p>今年已过去：<span class="J_past"></span></p>'
			+ '<p>距离明年还剩：<span class="J_had"></span></p>'
			);
	setInterval((function(){
		html.find(".J_date").html(T.strftime("%Y-%M-%D %H:%I:%S"));
		html.find(".J_gz"  ).html(T.magicInfo("GZ")+"年");
		html.find(".J_leap").html(T.isLeapYear()?"闰年":"平年");
		html.find(".J_sx"  ).html(T.magicInfo("SX"));
		html.find(".J_xz"  ).html(T.magicInfo("XZ"));
		html.find(".J_past").html(T.converter("{%d天}{%H时}{%I分}{%S秒}", new Date().getTime()-T.thisYear()));
		html.find(".J_had" ).html(T.converter("{%d天}{%H时}{%I分}{%S秒}", T.nextYear()-new Date().getTime()));
		return arguments.callee;
	})(), 1000);
	$("#date").html(html);
});